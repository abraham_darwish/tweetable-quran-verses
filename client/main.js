Template.hello.onRendered(() => {
  import Chart from 'chart.js';
  var ctx = document.getElementById("myChart").getContext('2d');
  var xaxis = new Array(55);
  var xaxisLabel = new Array(55);
  var backgroundColor = new Array(55);
  var borderColor = new Array(55);

  for (var i = 0; i < 56; i++) xaxis[i] = 0;
  for (var i = 0; i < 56; i++) xaxisLabel[i] = 10*i;
  for (var i = 0; i < 56; i++) {
    if (i < 14) {
      backgroundColor[i] = 'rgba(0, 0, 0, 0.4)';
      borderColor[i] = 'rgba(0, 0, 0, 1)';
    } else if (i < 28) {
      backgroundColor[i] = 'rgba(128, 128, 128, 0.4)';
      borderColor[i] = 'rgba(128, 128, 128, 1)';
    } else {      
      backgroundColor[i] = 'rgba(204, 0, 0, 0.4)';
      borderColor[i] = 'rgba(204, 0, 0, 1)';
    }
  }
  
    Meteor.call('fetchQuran', (error, results) => {
      if (error) {
        var errorData = error.details.response.data;
        console.log(errorData);
      } else {
        Session.set('quran', results.data);
      }
    });

    var quran = Session.get('quran');
    if (quran) {
      var data = quran.data;
      for (var index = 0; index < data.surahs.length; index++) {        
        for (var i = 0; i < data.surahs[index].ayahs.length; i++) {
          var ayah = data.surahs[index].ayahs;
          var currentAya = ayah[i];
          var charIndex = Math.round((currentAya.text.length) / 10);
          xaxis[charIndex]++;
          var valInArray = xaxis[charIndex];
        }        
      }
    }
    xaxis = xaxis.slice(0,56);
  
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: xaxisLabel,
      datasets: [{
        label: 'Tweetable Quran Verses',
        data: xaxis,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

  var totalNumVerses = 0;
  for (var i = 0; i < 56; i++) {
    totalNumVerses = xaxis[i] + totalNumVerses;
  }
  console.log('Total number of verses in the quran is ' + totalNumVerses);

  var tweetVerses140 = 0;
  for (var i = 0; i < 15; i++) {
    tweetVerses140 = xaxis[i] + tweetVerses140;
  }
  console.log('Number of verses that could be tweeted in 140 characters is ' + tweetVerses140);
  console.log('Percentage of verses that could be tweeted in 140 characters is ' + (tweetVerses140/totalNumVerses)*100);

  var tweetVerses280 = 0;
  for (var i = 15; i < 29; i++) {
    tweetVerses280 = xaxis[i] + tweetVerses280;
  }
  console.log('Number of verses that could be tweeted in 280 characters is ' + tweetVerses280);
  console.log('Percentage of verses that could be tweeted in 280 characters is ' + ((tweetVerses140+tweetVerses280)/totalNumVerses)*100);
});