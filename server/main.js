Meteor.methods({
  fetchQuran: () => {
    try {
      var quran = HTTP.call("GET", "http://api.alquran.cloud/quran/quran-uthmani");
      return quran;
    } catch (error) {
      throw new Meteor.Error("No Result", "Oops...", error);
    }
  }
});