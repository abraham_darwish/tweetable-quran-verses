# README #

### What is this repository for? ###

* This is a simple Meteor application using [ChartJS](http://www.chartjs.org/) and [Quran API](https://alquran.cloud/api) to show the number of tweetable verses in the Holy Quran before and after the new 280 character limit on twitter.
* Version 0.1
* [Peddle Hawk](https://www.peddlehawk.com/)

### How do I get set up? ###

* This is a meteor application so you can just download meteor, download this repo run the command 'meteor' in the terminal and it should work immediately
* Built with [ChartJS](http://www.chartjs.org/) and [Quran API](https://alquran.cloud/api)

### Contribution guidelines ###

* The code is yours do whatever you want with it!

### Who do I talk to? ###

* MIT License
* Need to build an app or a website? [Peddle Hawk](https://www.peddlehawk.com/) is for you! 